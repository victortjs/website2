$(function(){
	var data;


// Chama o arquivo com os links
var getData = $.ajax({
    url: 'https://www.mocky.io/v2/5a6bc16631000078341b8b77',
    success: function(r){
    	data = r;
    }
});

// Função para calcular o intervalo da data
var getTime = function(time){
	var msecPerMinute = 1000 * 60;
	var msecPerHour = msecPerMinute * 60;
	var msecPerDay = msecPerHour * 24;
	var msecPerYear = msecPerHour * 365;

	var date = new Date();
	var dateMsec = date.getTime();

	date.setTime(time);

	var interval = dateMsec - date.getTime();

	var years = Math.floor(interval / msecPerYear );
	interval = interval - (years * msecPerYear );

	var days = Math.floor(interval / msecPerDay );
	interval = interval - (days * msecPerDay );

	var hours = Math.floor(interval / msecPerHour );
	interval = interval - (hours * msecPerHour );

	var minutes = Math.floor(interval / msecPerMinute );
	interval = interval - (minutes * msecPerMinute );

	var seconds = Math.floor(interval / 1000 );

	if (years > 0) {
		return days + ' years ago';
	}
	if (days > 0) {
		return days + ' days ago';
	}
	if (hours > 0) {
		return hours + ' hours ago';
	}
	if (minutes > 0) {
		return minutes + ' minutes ago';
	}
	if (seconds > 0) {
		return seconds + ' seconds ago';
	}
}


// Função que monta o HTML da lista de links
getList = function(r){
	var r = '<div class="lista"><div class="votes"><p class="arrow"><i class="fas fa-angle-up"></i></p><p class="vote">' + r.upvotes + '</p></div><div class="global"><div class="url">' + r.meta.url + '</div><div class="title">' + r.meta.title + '</div><div class="inf"><p class="category">' + r.category + '</p><p style="background-color: #C7C7C7; width: 1px; height: 20px; margin: 0 12px;"></p><p class="photoUserMini"><a href=""><img src="images/foto_mini.jpg" style="vertical-align: middle;"> <span>' + r.meta.author + '</span></a></p><p class="date">' + (getTime(r.created_at) ? getTime(r.created_at) : '') + '</p><p style="font-weight: bold; font-size: 5px;"><i class="fas fa-circle"></i></p><p class="comments"><a href=""><i class="fas fa-comment"></i> <span>' + r.comments + ' Comments</span></a></p>' + (r.isOwner ? '<p class="edit"><a href="">Edit</a></p>' : '') + '</div></div></div>';

	return r;
}


// QUANDO TIVER CARREGADO
getData.done(function(){

	var el = $('#allLinks');

	// Add todos os links
	$.each(data.links, function(i, v){
		el.append(getList(v));
	});

	// função que ordena quando altera o Select
	$('#sortBy').on('change', function(){
		var value = this.value;

		var newData = data.links;

		switch(value) {
			case 'title':
				newData.sort(function(a,b){
					if(a.meta.title < b.meta.title) return -1;
					if(a.meta.title > b.meta.title) return 1;
					return 0;
				});
				break
			case 'upvotes':
				newData.sort(function(a,b){
					return b.upvotes - a.upvotes
				});
				break;
			case 'comments':
				newData.sort(function(a,b){
					return b.comments - a.comments
				});
				break;
			case 'created_at':
				newData.sort(function(a,b){
					return b.created_at - a.created_at
				});
				break;
		};


		$('#allLinks').html('');
		$.each(data.links, function(i, v){
			el.append(getList(v));
		});
	})

	// Função que busca o que digitar no campo de busca
	$('#search').on('keyup', function(){
		var value = this.value;

		var newData = $.grep(data.links, function(i, v){
			return i.meta.title.toLowerCase().indexOf(value) > -1;
		});

		$('#allLinks').html('');
		if(newData.length == 0) {
			$('#allLinks').html('<div class="noData">Nenhum link foi encontrado.</div>');
		}
		$.each(newData, function(i, v){
			el.append(getList(v));
		});

	});

	//el.append();
});

});